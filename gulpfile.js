//переменные

var gulp = require('gulp'), // Сообственно Gulp JS
  uglify = require('gulp-uglify'), // Минификация JS
  concat = require('gulp-concat'), // Склейка файлов
  concatCss = require('gulp-concat-css'),
  gutil = require('gulp-util'),
  noop = require("gulp-noop"),
  csso = require('gulp-csso'), // Минификация CSS
  sass = require('gulp-sass'), // Конвертация SASS (SCSS) в CSS
  clean = require('gulp-clean'), // очистка директорий
  autoprefixer = require('gulp-autoprefixer'), //автоматическая расстановка префиксов
  runSequence = require('run-sequence'), //синхронизация выполнения задач
  rigger = require('gulp-rigger'), //использование шаблонов
  csslint = require('gulp-csslint'),
  browserify = require('gulp-browserify'),
  es6ify = require('es6ify'),
  order = require('gulp-order'),
  connect = require('gulp-connect');

//пути
var path = {
  build: {
    html: 'build/website',
    js: 'build/website/js/',
    css: 'build/website/css/',
    img: 'build/website/img/',
    fonts: 'build/website/fonts/',
    browserify: 'build/website/js/'
  },
  src: {
    html_template: 'src/website/views/index.html',
    lint_config:'lint_config.json'
  },
  watch: {
    html: 'src/website/views/**/*.html',
    js: 'src/website/scripts/**/*.js',
    babel: 'src/website/scripts/**/*.js',
    style: 'src/website/css/**/*.scss',
    img: 'src/website/img/**/*.*',
    fonts: 'src/website/fonts/**/*.*',
    browserify: 'src/website/scripts/app.js'
  },
  order:{
    styleVariables: 'variables/**/*.scss',
    styleMixins: 'mixins/**/*.scss',
    styleReset: 'vendor/reset.scss'
  },
  clean: 'build/website/'
};

var environment = gutil.env.env || 'development';

var isProduction = environment === 'production';
var port = gutil.env.port || 1337;

// Задача clean
gulp.task('clean', function () {
  return gulp.src(path.clean, {read: false})
      .pipe(clean());
});

//connect dev server
gulp.task('connect', function() {
  connect.server({
    host: '127.0.0.1',
    port: port,
    root: 'build/website',
    livereload: true
  });
});

// Задача html
gulp.task('html', function() {
  gulp.src(path.watch.html)
    .pipe(rigger())
    .pipe(gulp.dest(path.build.html))
    .pipe(connect.reload());
});

// Задача css
gulp.task('css', function() {
  gulp.src(path.watch.style)
    .pipe(order([
      path.order.styleVariables,
      path.order.styleMixins,
      path.order.styleReset
    ]))
    .pipe(concat("style.scss"))
    .pipe(sass().on('error', sass.logError))
    .pipe(autoprefixer({
      browsers: ['last 3 versions'],
      cascade: false
    }))
    .pipe(concatCss("style.min.css", {rebaseUrls: false}))
    .pipe(isProduction ? csso() : noop())
    .pipe(gulp.dest(path.build.css))
    .pipe(connect.reload());
    // .pipe(csslint())
    // .pipe(csslint.reporter());
});

// Задача js
gulp.task('js', function() {
    gulp.src(path.watch.browserify)
      .pipe(browserify({
        insertGlobals : true,
        debug : (environment != 'production') ? 'production' : 'development',
        add: es6ify.runtime,
        transform: es6ify
      }))
      .pipe(isProduction ? uglify() : noop())
      .pipe(gulp.dest(path.build.browserify))
      .pipe(connect.reload());
});

// Задача img
gulp.task('img', function() {
  gulp.src(path.watch.img)
    .pipe(gulp.dest(path.build.img))
    .pipe(connect.reload());
});

// Задача font
gulp.task('font', function() {
  gulp.src(path.watch.fonts)
    .pipe(gulp.dest(path.build.fonts))
    .pipe(connect.reload());
});

// Задача watch
gulp.task('watch', function() {
  gulp.watch(path.watch.html, ['html']);
  gulp.watch(path.watch.style, ['css']);
  gulp.watch(path.watch.js, ['js']);
  gulp.watch(path.watch.img, ['img']);
  gulp.watch(path.watch.font, ['font']);
});

//список задач 
var tasks = {
  development: ['html', 'img', 'js', 'font', 'connect', 'watch'],
  build: ['html', 'img', 'font'],
  production: ['html', 'css', 'img', 'js', 'font']
};

gulp.task('default', function(callback) {
  runSequence('clean', 'css', tasks[environment], callback);
});

gulp.task('build', function(callback) {
  runSequence('clean', 'css', 'js', tasks['build'], callback);
});