FROM node:8.9.3 AS build-env

WORKDIR /src
ADD . /src

RUN yarn --no-progress install
RUN yarn --no-progress global add gulp

RUN gulp build --env=production

FROM kyma/docker-nginx

COPY --from=build-env ./src/build/website /var/www
CMD 'nginx'