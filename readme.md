# Gulp template for instant layout start #

This template make front-end development simple and sugar-like.

This repo has some branches:

### master ###
 Simple layout using modern packing technologies

* gulp with connect dev server and hot reload
* html: make html from siparate blocks by 
* css: convert scss to css with variables and mixins, 
   autoprefix & concat to one css file
* js: add es6 modules & other features, you can write moduleness app!!!
* font
* img

### reactWebpack ###
 Include react/redux SPA template with paging, navigation, device 
 checking and other fetures.

* gulp: for css,html,img and other tasks
* webpack: as gulp task for react/redux and hot modules reloading
* html:
* css: convert scss to css with variables and mixins, autoprefix 
  & concat to one css file & hotreload with dev-server
* js: understand jsx, es6 syntax for react/redux
* img
* font

## Installation ##
use console:


* pull or copy this repo
* `npm install`

## Usage ##
use console:

* `gulp` for development
* `gulp build --env=production` for production build

## Contributing ##
1. Fork it!
2. Create your feature branch: `git checkout -b my-new-feature`
3. Commit your changes: `git commit -am 'Add some feature'`
4. Push to the branch: `git push origin my-new-feature`
5. Submit a pull request :D

## History ##
TODO: Write history

## Credits ##
TODO: Write credits

## License ##
TODO: Write license

![habrahabr](https://habrastorage.org/files/1fb/08e/0a3/1fb08e0a34354419ab9b8ebbdecf5b18.png)